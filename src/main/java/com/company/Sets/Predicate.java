package com.company.Sets;

public interface Predicate<T> {
    boolean test(T Element);
}

