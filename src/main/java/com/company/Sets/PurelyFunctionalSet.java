package com.company.Sets;

public interface PurelyFunctionalSet<T> {
    boolean contains(T Element);
}
